import Game from './game';

export default class Character {
    protected _game: Game;
    public x: number;
    public y: number;
    public height: number;
    public width: number;
    protected canvas: HTMLCanvasElement;

	protected constructor(game?: Game, x?: number, y?: number, width?:number, height?:number) {
        this.canvas = <HTMLCanvasElement>document.getElementById('canvas');
        this._game = game;
        this.height = height;
        this.width = width;
        this.x = x;
        this.y = y;
	}
    public create() {
        let ctx = this.canvas.getContext("2d");
        ctx.fillRect(this.x,this.y,this.width,this.height);
    }

}