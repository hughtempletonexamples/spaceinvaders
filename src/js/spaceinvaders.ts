declare var require: any;
require('../css/main.css');
const config = require('../config/config.json');

import Game from './game';

class Spaceinvaders {
	private _game: Game;
	public fps: number;
	public now: number;
	public then: number;
	public interval: number;
	public delta: number;

	constructor(game: Game) {
		this._game = game;
		this.fps = 60;
		this.now;
		this.then = Date.now();
		this.interval = 1000/this.fps;
		this.delta;
	}

	public setup(): void {
		this.gameLoop();
	}

	private gameLoop(): void {
		requestAnimationFrame(this.gameLoop.bind(this));
		this.now = Date.now();
		this.delta = this.now - this.then;
		if (this.delta > this.interval) {
			this.then = this.now - (this.delta % this.interval);			 
			this._game.render();
		}
	}
}

window.onload = () => {

	let maingame = new Game(config);
	
	let spaceinvaders = new Spaceinvaders(maingame);
	spaceinvaders.setup();

}
