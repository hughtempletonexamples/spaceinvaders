import Character from './character';
import Game from './game';
import Bullet from './bullet';

export default class Enemy extends Character {

    private listenerHolder: boolean[];
    public bulletHolder: Bullet[];
    private enemySpeed: number;
    private hasBullet: boolean;
    private _bullet: Bullet;
    public active: boolean;
    public down: boolean;
    public direction: string;
    private interval: number;
    private rateOfFire: number;

    constructor(game?: Game, x?: number, y?: number, width?:number, height?:number) {
        super(game, x, y, width, height);
        this.listenerHolder = [];
        this.bulletHolder = [];
        this.enemySpeed = this._game.level / 3;
        this.hasBullet = false;
        this.down = false;
        this.active = true;
        this.rateOfFire = 120;
        this.direction = "right";
        this.switchRateOfFire();
        this.create();
        this._game.addAnims(this.isMove.bind(this));
        this.fireBullet();
    }

    public destroy(){
        clearInterval(this.interval);
    }

    private fireBullet(){

        let timeInterval = Math.floor((Math.random() * 150) + 100);
        this.interval = setInterval(function () {
            this.hasBullet = false;
            if(!this.active){
                clearInterval(this.interval);
            }
            for(var i = 0; i < this.bulletHolder.length; i++){
                if(this.bulletHolder[i].active){
                    this.hasBullet = true;
                }else{
                    var index = this.bulletHolder.indexOf(this.bulletHolder[i]);
                    if (index > -1) {
                        this.bulletHolder.splice(index, 1);
                    }
                }
            }
            if (!this.hasBullet && this.active){
                let type = "enemy" 
                let height = this.height / 5;
                let width = this.width / 5;
                let x = (this.x-width /2) + (this.width / 2);
                let y =  (this.y-height /2 ) + (this.height / 2);
                this._bullet = new Bullet(this._game, x, y, width, height, type);
                this.bulletHolder.push(this._bullet);
            }

        }.bind(this), timeInterval * this.rateOfFire);
        
    }

    private switchRateOfFire(){
        switch(this._game.level) { 
            case 4: { 
                this.rateOfFire = 100;
                break; 
            } 
            case 8: { 
                this.rateOfFire = 90;
                break; 
            } 
            case 10: {
                this.rateOfFire = 80; 
                break;    
            } 
            case 12: {
                this.rateOfFire = 60; 
                break;    
            } 
            default: { 
                this.rateOfFire = 120;
                break;              
            } 
        }
    }
        
    private moveLeft() {
        if (this.x >= 0) {
            this.x -= this.enemySpeed;
        }else{
            this.down = true;
        }
    }
    private moveRight() { 
        if (this.x <= this.canvas.width - this.width) {
            this.x += this.enemySpeed;
        }else{
            this.down = true;
        }
    }
    public setMoveDown(moveDown:boolean){
        this.down = moveDown;
    }
    public getMoveDown(){
        return this.down;
    }
    private isMove(){
        if(this.down){
            this.y += this.height * 2 ;
            //this.draw();
            this.down = false;
            if (this.direction === "right") {
                this.direction = "left";     
            } else {
                this.direction = "right";
            }
        }
        if (this.direction === "right") {
            this.moveRight();       
        } else {
            this.moveLeft();
        }
        this.draw();
        return this.active;
    }
    
    private draw(){
        let x = this.x *= this._game.xRatio;
        let y = this.y *= this._game.yRatio;
        let halfScreen = this.canvas.width / 2;
        let ctx = this.canvas.getContext("2d");
        var my_gradient=ctx.createLinearGradient(0,0,halfScreen,0);
        my_gradient.addColorStop(0,"blue");
        my_gradient.addColorStop(0.5,"red");
        my_gradient.addColorStop(1,"green");
        ctx.fillStyle=my_gradient;
        ctx.fillRect(x,y,this.width,this.height);
    }
}