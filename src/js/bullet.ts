import Game from './game';
import Character from './character';

export default class Bullet {
    protected _game: Game;
    protected _character: Character;
    public x: number;
    public y: number;
    public height: number;
    public width: number;
    protected type: string;
    protected canvas: HTMLCanvasElement;
    public active:boolean;
    protected bulletSpeed: number;

    constructor(game?: Game, x?: number, y?: number, width?:number, height?:number, type?:string) {
        this.canvas = <HTMLCanvasElement>document.getElementById('canvas');
        this.type = type;
        this._game = game;
        this.height = height;
        this.width = width;;
        this.x = x;
        this.y = y; 
        this.bulletSpeed = 5;
        this.active = true;
        if(this.type === "player"){
            this.bulletSpeed = 6;
            this._game.addAnims(this.moveUp.bind(this));
        }else{
            this._game.addAnims(this.moveDown.bind(this));
            this.bulletSpeed = 4;
        }
    }
    public getActive(){
        return this.active;
    }
    private draw(){
        let x = this.x *= this._game.xRatio;
        let y = this.y *= this._game.yRatio;
        let ctx = this.canvas.getContext("2d");
        ctx.fillRect(x,y,this.width,this.height);
    }
    private moveDown() {
        if(this.y < this.canvas.height){
            this.y += this.bulletSpeed;
            //console.log("moveUp: "+this.x + "-" +this.y+" | " + this.width + "-"+this.height);
            this.draw();
        }else{
            return this.active = false;
        }
        return this.active;
    }
    private moveUp() {  
        if(this.y > 0){
            this.y -= this.bulletSpeed;
            //console.log("moveUp: "+this.x + "-" +this.y+" | " + this.width + "-"+this.height);
            if(this.active){
                this.draw();
            }
        }else{
            return this.active = false;
        }
        return this.active;
    }

}