import Player from './player';
import Enemy from './enemy';

interface configData {
	enemysData: [{
		x:number;
		y:number;
		width:number;
		height:number;
	}];
	characterData: {
		x:number;
		y:number;
		width:number;
		height:number;
	};
}

export default class Game {
	public _canvas: HTMLCanvasElement;
	private _ctx: CanvasRenderingContext2D;
	private height: number = window.innerHeight;
	private width: number = window.innerWidth;
	private animHolder: Function[];
	private playersHolder: Player[];
	private enemiesHolder: Enemy[];
	private config: configData;
	private live: boolean;
	public lives: number;
	private score: number;
	public level: number;
	private countdown: string;
	public xRatio: number;
	public yRatio: number;

	constructor(config:configData) {
		this._canvas = <HTMLCanvasElement>document.getElementById('canvas');
		this.config = config;
		this._canvas.width = this.width;
		this._canvas.height = this.height;
		this.xRatio = 1;
		this.yRatio = 1;
		this._ctx = this._canvas.getContext("2d");
		this.animHolder = [];
		this.playersHolder = [];
		this.enemiesHolder = [];
		this.live = true;
		this.lives = 3;
		this.score = 0;
		this.level = 1;
		this.countdown = "5";
		this.drawDialog();
		this.setPlayer();
		this.setEnemies();
	}

	private drawDialog(){
		let ctx = this._canvas.getContext("2d");
		//score
		ctx.font = "30px Comic Sans MS";
		ctx.fillStyle = "white";
		ctx.textAlign = "center";
		ctx.fillText("Lives: x" + this.lives,200,50,200); 
		//points
		ctx.font = "30px Comic Sans MS";
		ctx.fillStyle = "white";
		ctx.textAlign = "center";
		ctx.fillText("Level: " + this.level,this._canvas.width/2,50,200); 
		//level
		ctx.font = "30px Comic Sans MS";
		ctx.fillStyle = "white";
		ctx.textAlign = "center";
		ctx.fillText("Score: " + this.score,this._canvas.width-200,50,200); 

		document.addEventListener('resize', function(){
			this.resizeCanvas();
		});
	}

	private resizeCanvas(){
		let width = window.innerWidth;
		let height = window.innerHeight;
		this.xRatio = width / this._canvas.width;
		this.yRatio = height / this._canvas.height;
		this._canvas.width = width;
		this._canvas.height = height;
		this.addText();
	}
	
	private addText(){
		let ctx = this._canvas.getContext("2d");
		ctx.font = "30px Comic Sans MS";
		ctx.fillStyle = "red";
		ctx.textAlign = "center";
		ctx.fillText("addText in", this._canvas.width/2, this._canvas.height/2);
		ctx.font = "30px Comic Sans MS";
		ctx.fillStyle = "red";
		ctx.textAlign = "center";
		ctx.fillText(this.countdown, this._canvas.width/2, this._canvas.height/2); 
	}

	private gameOver(){
		let ctx = this._canvas.getContext("2d");
		ctx.font = "30px Comic Sans MS";
		ctx.fillStyle = "red";
		ctx.textAlign = "center";
		ctx.fillText("Game Over", this._canvas.width/2, this._canvas.height/2); 
	}

	public addPlayers(player:Player): void {
		this.playersHolder.push(player);
	}

	public addEnemys(enemy:Enemy): void {
		this.enemiesHolder.push(enemy);
	}

	public addAnims(anim:Function): void {
		this.animHolder.push(anim);
	}

	public render(): void {
		//console.log('rendering');
		if(this.live){
			this._ctx.clearRect(0, 0, this.width, this.height);
			this.drawDialog();
			this.enemyCollaberation();
			this.moveAnims();
			this.collisions();
		}
	}

	private setEnemies(){

		for(let i = 0; i < this.enemiesHolder.length; i++){
			let enemy = this.enemiesHolder[i];
			enemy.active = false;
			enemy.destroy();
		}
		this.enemiesHolder = [];

		let newLine;
		let enemy;
		let y = this.config.enemysData[0].height;
		let setx = this.config.enemysData[0].width * 2;
		let x = setx;
		var rowCount = 0;

		for (let i = 0; i < this.config.enemysData.length; i++) {
			enemy = this.config.enemysData[i];
			enemy.width = this._canvas.width / 20;
			enemy.height = this._canvas.height / 20;
			newLine = x + (enemy.width * 3);
			if(newLine > this._canvas.width){
				y += enemy.width;
				x = setx;
				rowCount++;
			}
			if(rowCount < 2){
				let enemyObj = new Enemy(this, x, y, enemy.width, enemy.height);
				this.addEnemys(enemyObj);
			}
			x += enemy.width + (enemy.width/2);
		}
	}

	private setPlayer(){
		for(let i = 0; i < this.playersHolder.length; i++){
			let player = this.playersHolder[i];
			player.active = false;
			player.destroy();
		}
		this.playersHolder = [];
		this.config.characterData.width = this._canvas.width / 20;
		this.config.characterData.height = this._canvas.height / 20;
		let playerObj = new Player(this, this.config.characterData.x, this.config.characterData.y, this.config.characterData.width, this.config.characterData.height);
		this.addPlayers(playerObj);
	}

	private moveAnims(){
		for(let i = 0; i < this.animHolder.length; i++){
			let active = this.animHolder[i]();
			if(!active){
				this.animHolder[i] = undefined;
				let index = this.animHolder.indexOf(this.animHolder[i]);
				if (index > -1) {
					this.animHolder.splice(index, 1);
				}
			}
		}
	}

	private enemyCollaberation(){
		let moveAll = false;
		for(let i = 0; i < this.enemiesHolder.length; i++){
			let enemy = this.enemiesHolder[i];
			if(enemy.getMoveDown()){
				for(let j = 0; j < this.enemiesHolder.length; j++){
					let enemyToSet = this.enemiesHolder[j];
					enemyToSet.setMoveDown(true);
				}
			}
		}
	}

	private resetAll(){
		if(this.live){
			this.setEnemies();
			this.addText();
		}else{
			this.playersHolder = [];
			this.enemiesHolder = [];
			this.animHolder = [];
			this.gameOver();
		}
	}

	private collisions(){
		for(let i = 0; i < this.enemiesHolder.length; i++){
			let enemy = this.enemiesHolder[i];
			for(let j = 0; j < this.playersHolder.length; j++){
				let player = this.playersHolder[j];
				this.bulletCollisions(player, enemy)
				this.characterHit("player", player, enemy);
				this.characterHit("enemy", player, enemy);
				this.playerCollision(player, enemy);
			}

		}
	}

	private playerCollision(player: Player, enemy: Enemy){
		if(player.x < enemy.x + enemy.width &&
			player.x + player.width > enemy.x &&
			player.y < enemy.y + enemy.height &&
			player.y + player.height > enemy.y) {
				if(this.lives === 0){
					this.live = false;
				}
				enemy.active = false;
				this.resetAll();
				this.lives--;
				return;
			}
	}

	private bulletCollisions(player: Player, enemy: Enemy){
		for(let j = 0; j < player.bulletHolder.length; j++){
			let playerBullet = player.bulletHolder[j];
			for(let i = 0; i < enemy.bulletHolder.length; i++){
				let enemyBullet = enemy.bulletHolder[i];
				if(playerBullet.x < enemyBullet.x + enemyBullet.width &&
					playerBullet.x + playerBullet.width > enemyBullet.x &&
					playerBullet.y < enemyBullet.y + enemyBullet.height &&
					playerBullet.y + playerBullet.height > enemyBullet.y) {
						playerBullet.active = false;
						player.bulletHolder = [];
						enemyBullet.active = false;
						enemy.bulletHolder = [];
						return;
					}
			}
		}
	}

	private characterHit(which: String, player: Player, enemy: Enemy){
		if(which === "player"){
			for(let i = 0; i < enemy.bulletHolder.length; i++){
				let enemyBullet = enemy.bulletHolder[i];
				if(enemyBullet.active){
					if(player.x < enemyBullet.x + enemyBullet.width &&
						player.x + player.width > enemyBullet.x &&
						player.y < enemyBullet.y + enemyBullet.height &&
						player.y + player.height > enemyBullet.y) {
							
							enemyBullet.active = false;
							if(this.lives === 0){
								
								//only for multiplayer
								// let playerIndex = this.playersHolder.indexOf(player);
								// if (playerIndex > -1) {
								// 	this.playersHolder.splice(playerIndex, 1);
								// }
								this.live = false;
								this.resetAll();
							}
							this.lives--;
							return;
						}
				}
			}
		}else{
			for(let i = 0; i < player.bulletHolder.length; i++){
				let playerBullet = player.bulletHolder[i];
				if(playerBullet.active){
					if(enemy.x < playerBullet.x + playerBullet.width &&
						enemy.x + enemy.width > playerBullet.x &&
						enemy.y < playerBullet.y + playerBullet.height &&
						enemy.y + enemy.height > playerBullet.y) {
							enemy.active = false;
							this.score += 200;
							let enemyIndex = this.enemiesHolder.indexOf(enemy);
							if (enemyIndex > -1) {
								this.enemiesHolder.splice(enemyIndex, 1);
							}
							playerBullet.active = false;
							if(this.enemiesHolder.length === 0){
								this.level++;
								this.resetAll();
							}
							return;
						}
				}
			}
		}
	}

	
}

