import Character from './character';
import Game from './game';
import Bullet from './bullet';

interface eventsData {
    type: string;
    eventFn: EventHandlerNonNull;
}

export default class Player extends Character {

    private listenerHolder: boolean[];
    public bulletHolder: Bullet[];
    private playerSpeed: number;
    private hasBullet: boolean;
    private _bullet: Bullet;
    public active: boolean;
    private events: eventsData[];
    private colours: string[];

    constructor(game?: Game, x?: number, y?: number, width?:number, height?:number) {
        super(game, x, y, width, height);
        this.listenerHolder = [];
        this.bulletHolder = [];
        this.playerSpeed = 5;
        this.hasBullet = false;
        this.active = true;
        this.events = [];
        this.colours = ["red", "yellow", "orange", "green"];
        this.y = this.canvas.height - this.height;
        this.create();
        this.addListeners();
        this._game.addAnims(this.isMove.bind(this));
    }

    public destroy(){
        for(var i = 0; i < this.events.length; i++){
            let event = this.events[i];
            document.removeEventListener(event.type,event.eventFn);
        }
    }
        
    private moveLeft() {
        if (this.x >= 0) {
            this.x -= this.playerSpeed;
            //console.log("here: " + this.x);
        }
    }
    private moveRight() { 
        if (this.x <= this.canvas.width - this.width) {
            this.x += this.playerSpeed;
        }
    }

    private draw(){
        let x = this.x *= this._game.xRatio;
        let y = this.y *= this._game.yRatio;
        let ctx = this.canvas.getContext("2d");
        ctx.restore();
        ctx.fillStyle = this.colours[this._game.lives];
        ctx.fillRect(x,y,this.width,this.height);
    }

    private addKeyPressDown( keyCode: number , myFunction:Function ) {
        document.addEventListener("keydown", downListener, true);
        function downListener (e : KeyboardEvent) {
            e.preventDefault();
            if (e.keyCode === keyCode) {
                myFunction(keyCode);
            }
        }
        var eventData = {
            type:"keydown",
            eventFn: downListener
        }
        this.events.push(eventData);
    };

    private addKeyPressUp( keyCode: number , myFunction:Function ) {
        document.addEventListener("keyup", upListener, true);
        function upListener (e : KeyboardEvent) {
            e.preventDefault();
            if (e.keyCode === keyCode) {
                myFunction(keyCode);
            }
        }
        var eventData = {
            type:"keyup",
            eventFn: upListener
        }
        this.events.push(eventData);
    };

    private listenerDown(keyCode:number){
        this.listenerHolder[keyCode] = true;
    }
    private listenerUp(keyCode:number){
        this.listenerHolder[keyCode] = false;
    }

    public addListeners(){
        this.addKeyPressDown( 37 , this.listenerDown.bind(this));
        this.addKeyPressUp( 37 , this.listenerUp.bind(this));
        this.addKeyPressDown( 39 , this.listenerDown.bind(this));
        this.addKeyPressUp( 39 , this.listenerUp.bind(this));
        this.addKeyPressDown( 32 , function (){
            this.hasBullet = false;

            for(var i = 0; i < this.bulletHolder.length; i++){
                //console.log(this.bulletHolder[i].active);
                if(this.bulletHolder[i].active){
                    this.hasBullet = true;
                }else{
                    var index = this.bulletHolder.indexOf(this.bulletHolder[i]);
                    if (index > -1) {
                        this.bulletHolder.splice(index, 1);
                    }
                }
            }

            if (!this.hasBullet){
                //console.log("hut");
                let type = "player" 
                let height = this.height / 5;
                let width = this.width / 5;
                let x = (this.x-width /2) + (this.width / 2);
                let y =  this.y - ((this.height/2) - height);
                this._bullet = new Bullet(this._game, x, y, width, height, type);
                this.bulletHolder.push(this._bullet);
            }

        }.bind(this));
    }

    public isMove(){
        if(this.listenerHolder[37]){
            this.moveRight();
        }else if(this.listenerHolder[39]){
            this.moveLeft();
        }
        if(this.active){
            this.draw();
        }
        return this.active;
    }
}